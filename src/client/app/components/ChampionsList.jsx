import React from 'react';
import GridList from 'material-ui/lib/grid-list/grid-list';
import GridTile from 'material-ui/lib/grid-list/grid-tile';

import { DB_URLS, RIOT_URLS, FILE_EXTENSIONS } from '../common/urls';

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    }
};

export default class ChampionsList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            championsList: [],
            loaded: false,
        };
    }

    getChampionList() {
        return fetch(DB_URLS.championList)
            .then(response => response.json())
            .then(championList => this.setState({
                championList: championList,
                loaded: true
            }));
    }

    componentWillMount(){
        this.getChampionList();
    }

    componentWillReceiveProps(nextProps){
        this.imageHostUrl = RIOT_URLS.host + nextProps.version + RIOT_URLS.images.champion;
    }

    render() {
        if (!this.state.loaded) {
            return this.renderLoadingView();
        }

        const championList = this.state.championList.map(champion => {
            return (
                <GridTile
                    key={champion.id}
                    title={champion.name}
                    subtitle={(<span><b>{champion.title}</b></span>)}
                >
                    <img src={this.imageHostUrl + champion.key + FILE_EXTENSIONS.png} />
                </GridTile>
            );
        });

        return (
            <div style={styles.root}>
                <GridList
                    cellHeight={150}
                    cols={8}
                >
                    {championList}
                </GridList>
            </div>
        );
    }

    renderLoadingView() {
        return (
            <div>
                <span> Loading champions... </span>
            </div>
        );
    }
}

ChampionsList.propTypes = {
    version: React.PropTypes.string.isRequired
};
