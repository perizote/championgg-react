import React from 'react';
import { DB_URLS } from '../common/urls';
import ChampionsList from './ChampionsList';

export default class App extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            version: ''
        };
    }

    getPatchVersion() {
        return fetch(DB_URLS.patchVersion)
            .then(response => response.json())
            .then(version => this.setState({
                version: version.ddPatch
            }));
    }

    componentWillMount(){
        this.getPatchVersion();
    }

    render() {
        return (
            <div>
                <ChampionsList version={this.state.version}></ChampionsList>
            </div>
        );
    }
}
