const DB_HOST = 'src/client/app/db';

export const DB_URLS = {
    patchVersion: DB_HOST + '/dd_patch.json',
    championList: DB_HOST + '/champions.json',
    items: DB_HOST + '/items.json',
    skills: DB_HOST + '/skills.json',
    masteries: DB_HOST + '/masteries.json',
    summoners: DB_HOST + '/summoners.json',
    championPages: DB_HOST + '/webchampionpages.json',
    statisticspages: DB_HOST + '/webstatisticspages.json'
};

export const RIOT_URLS = {
    host: 'https://ddragon.leagueoflegends.com/cdn/',
    images: {
        champion: '/img/champion/'
    }
};

export const FILE_EXTENSIONS = {
    png: '.png'
}
