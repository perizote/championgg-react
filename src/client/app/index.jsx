import injectTapEventPlugin from 'react-tap-event-plugin';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './styles/main.css';

injectTapEventPlugin();
main();

function main() {
    const app = document.createElement('div');
    app.id = 'content';
    document.body.appendChild(app);
    ReactDOM.render(<App />, app);
}
