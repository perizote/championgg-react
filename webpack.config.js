var path = require('path');
var HtmlwebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');

const TARGET = process.env.npm_lifecycle_event;
process.env.BABEL_ENV = TARGET;

const PATHS = {
    app: path.join(__dirname, 'src/client/app'),
    build: path.join(__dirname, 'src/client/build'),
    node_modules: path.join(__dirname, 'node_modules')
};

module.exports = {
    entry: PATHS.app,
    resolve: {
        extensions: ['', '.js', '.jsx', '.css'],
        modulesDirectories: [PATHS.node_modules, PATHS.app]
    },
    output: {
        path: PATHS.build,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loaders: ['style', 'css'],
            },
            {
                test: /\.jsx?$/,
                loaders: ['babel'],
                include: PATHS.app
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                loaders: ['url']
            }
        ]
    },
    devServer: {
        hot: true,
        inline: true,
        progress: true,
        historyApiFallback: true,
        stats: 'errors-only',
        port: process.env.PORT,
        host: process.env.HOST
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlwebpackPlugin({
            title: 'ChampionGG'
        })
    ]
};
